﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPIdentityMVC.Enum
{
    public enum Role
    {
        SuperAdmin,
        Admin,
        Moderator,
        Basic
    }
}
